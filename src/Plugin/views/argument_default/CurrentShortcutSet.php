<?php

namespace Drupal\shortcut_view\Plugin\views\argument_default;

use Drupal\Core\Cache\Cache;
use Drupal\Core\Cache\CacheableDependencyInterface;
use Drupal\views\Plugin\views\argument_default\ArgumentDefaultPluginBase;

/**
 * Default argument plugin to extract the shortcut set assigned to current user.
 *
 * @ViewsArgumentDefault(
 *   id = "current_shortcut_set",
 *   title = @Translation("Shortcut set assigned to current user")
 * )
 */
class CurrentShortcutSet extends ArgumentDefaultPluginBase implements CacheableDependencyInterface {

  /**
   * {@inheritdoc}
   */
  public function getArgument() {
    /** @var \Drupal\shortcut\ShortcutSetInterface $shortcut_set */
    $shortcut_set = shortcut_current_displayed_set();
    return $shortcut_set->id();
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheMaxAge() {
    return Cache::PERMANENT;
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheContexts() {
    // @todo it needs to be cached per user.shortcut_set cache context.
    return ['user'];
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheTags() {
    return [];
  }

}
